import os
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL) 
counter = 1
default_size = 1024
for x in range(15, 297):
    tot = counter * default_size
    cmd = f'base64 /dev/urandom | head -c {tot} > {x}.txt'
    os.system(cmd)
    counter = counter + 1
    if counter == 11:
        counter = 1
