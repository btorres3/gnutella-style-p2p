PYTHON   := python3.7
PIP      := python3-pip
TMUX     := tmux
TELNET   := telnet
SRC      := ./gnutella
NODE     := ${SRC}/node.py
SP       := ${SRC}/super_peer.py
TOPOLOGY := a

default:
	@$(PYTHON) $(SP) 0 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 1 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 2 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 3 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 4 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 5 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 6 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 7 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 8 $(TOPOLOGY) &
	@$(PYTHON) $(SP) 9 $(TOPOLOGY) &

	@$(PYTHON) $(NODE) 10 0 &
	@$(PYTHON) $(NODE) 11 0 &
	@$(PYTHON) $(NODE) 12 0 &
	@$(PYTHON) $(NODE) 13 1 &
	@$(PYTHON) $(NODE) 14 1 &
	@$(PYTHON) $(NODE) 15 1 &
	@$(PYTHON) $(NODE) 16 2 &
	@$(PYTHON) $(NODE) 17 2 &
	@$(PYTHON) $(NODE) 18 2 &
	@$(PYTHON) $(NODE) 19 3 &
	@$(PYTHON) $(NODE) 20 3 &
	@$(PYTHON) $(NODE) 21 3 &
	@$(PYTHON) $(NODE) 22 4 &
	@$(PYTHON) $(NODE) 23 4 &
	@$(PYTHON) $(NODE) 24 4 &
	@$(PYTHON) $(NODE) 25 5 &
	@$(PYTHON) $(NODE) 26 5 &
	@$(PYTHON) $(NODE) 27 5 &
	@$(PYTHON) $(NODE) 28 6 &
	@$(PYTHON) $(NODE) 29 6 &
	@$(PYTHON) $(NODE) 30 6 &
	@$(PYTHON) $(NODE) 31 7 &
	@$(PYTHON) $(NODE) 32 7 &
	@$(PYTHON) $(NODE) 33 7 &
	@$(PYTHON) $(NODE) 34 8 &
	@$(PYTHON) $(NODE) 35 8 &
	@$(PYTHON) $(NODE) 36 8 &
	@$(PYTHON) $(NODE) 37 9 &
	@$(PYTHON) $(NODE) 38 9 &
	@$(PYTHON) $(NODE) 39 9 &

kill:
	@pkill -f "gnutella"

prepare-dev:
	@sudo dnf install $(TMUX) $(TELNET)

