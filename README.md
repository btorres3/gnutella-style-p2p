# Gnutella-style Peer-to-Peer File Sharing system

File sharing system using a Gnutella-style peer-to-peer network architecture, developed in Python.
