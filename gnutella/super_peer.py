import os
import sys
import json
import time
import socket
import threading
import xmlrpc.client
from xmlrpc.client import ServerProxy
from socketserver import ThreadingMixIn
from xmlrpc.server import SimpleXMLRPCServer

class SimpleThreadedXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
    """Adds multithreading to Python's SimpleXMLRPCServer
    """
    pass

class SuperPeer(object):
    """Creates a super-peer for the network

    Keyword arguments:
    id        -- its unique ID
    nodes     -- list of leaf-nodes its responsible for
    neighbors -- list of its super-peer neighbors
    """
    def __init__(self, id, nodes, neighbors):
        self.id = id
        self.nodes = nodes
        self.neighbors = neighbors

        self.files = {}
        self.msgs = [[] for _ in range(50)]
        self.folder = "./resources/SP" + str(id)

        # Assign one thread to clear its msgs list every 30 seconds
        threading.Thread(target=self._update,
                         args=(), daemon=False).start()
        # Assign one thread to run the RPC server
        threading.Thread(target=self._run_rpc_server,
                         args=(), daemon=False).start()

    def _run_rpc_server(self):
        """Helper method that registers this class' functions and starts the RPC server"""
        sp_server = SimpleThreadedXMLRPCServer(
            ('', 9090 + self.id), allow_none=True, logRequests=False)
        sp_server.register_instance(self)
        sp_server.serve_forever()

    def search(self, msg_id, ttl, file_name):
        """RPC method that starts query() and uses topology info"""
        arr = []
        if self.id not in msg_id[1]:
            msg_id[1].append(self.id)
        
        if len(self.neighbors) > 1:
            arr.append(self.query(msg_id, ttl, file_name))

        for neighbor in self.neighbors:
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            arr.append(proxy.query(msg_id, ttl, file_name))
        
        return arr

    def query(self, msg_id, ttl, file_name):
        """Asks a super peer to see if it has file_name"""
        if ttl == 0:
            return None   
        ttl -= 1

        leaf_list = self.files.get(file_name, [])

        msg_id_list = [msg_id[0], msg_id[2]]
        if msg_id_list not in self.msgs:
            self.msgs.append(msg_id_list)
        else:
            print(f'Super-peer #{self.id}: I\'ve already seen this!')
            return None

        if msg_id[0] in self.nodes and leaf_list:
            return self.query_hit(msg_id, 10, file_name, 'localhost', 12345 + leaf_list[0])
        elif leaf_list and msg_id[0] not in self.nodes:
            neighbor = msg_id[1].pop()
            print(f'QUERY: Found it! Currently at {self.id} and going back to {neighbor} for QUERY_HIT')
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            ip = 'localhost'
            port = 12345 + leaf_list[0]
            return proxy.query_hit(msg_id, 10, file_name, ip, port)
        elif len(self.neighbors) == 1:
            neighbor = self.neighbors[0]
            print(f'QUERY: Currently at {self.id} and going to {neighbor}')
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            msg_id[1].append(self.id)
            return proxy.query(msg_id, ttl, file_name)
        else:
            return None

    def query_hit(self, msg_id, ttl, file_name, leaf_node_ip, leaf_port):
        """A super peer has file_name and will send back leaf_port in reverse order the request came"""
        if ttl == 0:
            return None   
        ttl -= 1
        if msg_id[0] not in self.nodes:
            neighbor = msg_id[1].pop()
            print(f'QUERY HIT: Currently at {self.id} and going back to {neighbor}')
            proxy = ServerProxy(f'http://localhost:909{neighbor}')
            return proxy.query_hit(msg_id, ttl, file_name, leaf_node_ip, leaf_port)
        elif msg_id[0] in self.nodes:
            return leaf_port

    def registry(self, node_id, node_files):
        """An RPC method that allows peers to register their files with the server."""
        for f in node_files:
            try:
                if node_id not in self.files[f]:
                    self.files[f].append(node_id)
            except KeyError:
                self.files[f] = [node_id]

        print(f"Leaf Node {node_id} has successfully registered, or reregistered, all its Files.")

    def _update(self):
        while True:
            time.sleep(30)
            self.msgs.clear()

# To start a super peer, it requires three command line arguments:
# Argument 1: the id of the peer itself
# Argument 2: the topology (a for all-to-all, l for linear)
# To get its neighbors and peers, I have a created a JSON file
# that statically allocates all this information, so its neighbors/peers
# are determined by its user-provided id and user-provided topology
try:
    sp_id = int(sys.argv[1])
    with open(r"./resources/nodes.json") as file:
        nodes_dict = json.load(file)
    info = nodes_dict.get(sys.argv[1])
    nodes = [int(i) for i in info.get('leaf_nodes')]
    if sys.argv[2] == "l":
        neighbors = [int(i) for i in info.get('linear')]
    elif sys.argv[2] == "a":
        neighbors = [int(i) for i in info.get('all_to_all')]
    sp = SuperPeer(sp_id, nodes, neighbors)
except ValueError:
    print("Please enter in a valid integer!")
    sys.exit(0)
