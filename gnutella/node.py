import os
import sys
import json
import time
import socket
import threading
import xmlrpc.client
from menu import Menu
from xmlrpc.client import ServerProxy
from xmlrpc.server import SimpleXMLRPCServer

class Node:
    """Creates a leaf-node for the network

    Keyword arguments:
    node_id       -- its unique ID
    super_peer_id -- the ID of its super-peer
    """
    def __init__(self, node_id, super_peer_id):
        self.node_id = node_id
        self.super_peer_id = super_peer_id

        (self.folder, self.files) = self._create_file_list()

        self.proxy = ServerProxy(f'http://localhost:909{super_peer_id}')
        self.proxy.registry(self.node_id, self.files)

        # Assign one thread to run the RPC server
        threading.Thread(target=self._run_rpc_server, args=(), daemon=True).start()
        # Assign one thread to run automatic updates
        threading.Thread(target=self._update, daemon=True).start()
        # The main thread will run the server-side of the peer
        self._run_server()
        
    def _create_file_list(self):
        """Helper method that creates the node's file array and folder variable"""
        folder = "./resources/" + "SP" + str(self.super_peer_id) + "/LN" + str(self.node_id)
        return (folder, os.listdir(folder))        

    def _run_rpc_server(self):
        """Helper method that registers this class' functions and starts the RPC server"""
        node_server = SimpleXMLRPCServer(('', 9090 + self.node_id))
        node_server.register_instance(self)
        node_server.serve_forever()

    def obtain(self, file_name):
        """Helper method that returns a file to another leaf-node"""
        file_to_send = self.folder + f"/{file_name}"
        with open(file_to_send, "rb") as handle:
            return xmlrpc.client.Binary(handle.read())
        
    def _run_server(self):
        """Run the server-side of the leaf node"""
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind(('', 12345 + self.node_id))
        server_socket.listen(5)

        while True:
            try:
                (client, _) = server_socket.accept()
                threading.Thread(target=self._client_handler,
                                 args=(client,)).start()
            except KeyboardInterrupt:
                print("Exiting...")
                sys.exit(0)

    def _client_handler(self, client):
        """Helper method that handles clients once they are connected"""
        menu = Menu(client, self.node_id, False, self.files, self.folder)
        seq_num = 0
        while True:
            choice = menu.menu()
            if choice == 1:
                file_name = menu.search_for_file()
                port_arr = self.proxy.search([self.node_id, [self.super_peer_id], seq_num], 10, file_name)
                seq_num += 1
                port_num = menu.choose_port(port_arr)
                if not port_num:
                    client.send(b'\r\nFile not found!\r\n\r\n')
                    continue

                client.send(b'Downloading now...\r\n\r\n')

                node_rpc_port = (port_num - 12345) + 9090
                proxy2 = ServerProxy(
                    f'http://localhost:{node_rpc_port}')
                
                place_to_save = self.folder + f"/{file_name}"

                with open(place_to_save, "wb") as handle:
                    handle.write(proxy2.obtain(file_name).data)

            elif choice == 2:
                menu.display()
            elif choice == 3:
                file_to_remove = menu.remove()
                if file_to_remove == 'q':
                    continue
                os.remove(f"{self.folder}/{file_to_remove}")

    def _update(self):
        """Helper method that updates each leaf nodes's directory every 1 second and prints deletions or additions"""
        while True:
            time.sleep(1)
            directory_to_track = os.listdir(self.folder)
            less = [item for item in self.files if item not in directory_to_track]
            more = [item for item in directory_to_track if item not in self.files]
            if less:
                print(f"File(s) deleted for leaf node {self.node_id}!")
                for item in less:
                    print(f"{item} removed.")
                    self.files.remove(item)
                    self.proxy.registry(self.node_id, self.files)

            elif more:
                print(f"File(s) added for leaf node {self.node_id}!")
                for item in more:
                    print(f"{item} added.")
                    self.files.append(item)
                    self.proxy.registry(self.node_id, self.files)

# To start a leaf-node, it requires two command line arguments:
# Argument 1: the id of the node itself
# Argument 2: the id of its super peer
try:
    n_id = int(sys.argv[1])
    sp_id = int(sys.argv[2])
    node = Node(n_id, sp_id)
except ValueError:
    print("Please enter in a valid integer!")
    sys.exit(0)
